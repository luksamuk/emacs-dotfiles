;; Use nasm-mode instead of asm-mode when opening .asm files
(add-to-list 'auto-mode-alist '("\\.asm\\'" . nasm-mode))
