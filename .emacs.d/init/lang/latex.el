;; Latex Preview Pane
;;(when (display-graphic-p)
;;  (latex-preview-pane-enable))

;; Compile current file.
;; Normally latex-preview-pane would handle this,
;; but if we have any trouble...
(defun rubber-compile-file ()
  (interactive)
  (shell-command
   (concat "rubber -d " buffer-file-name))
  (message "Finished LaTeX compilation."))


(setq TeX-view-program-selection '((output-pdf "Zathura")))
