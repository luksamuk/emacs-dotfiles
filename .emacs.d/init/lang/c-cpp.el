(require 'cc-mode)

;; Indentation, style, etc
(c-set-offset 'substatement-open 0)
(define-key c-mode-base-map (kbd "RET") 'newline-and-indent)
(defun my-c-mode-hook ()
  (setq c-basic-offset 4)
  (setq c-default-style "k&r")
  (c-set-offset 'substatement-open 0))
(add-hook 'c++-mode-hook 'my-c-mode-hook)
(add-hook 'c-mode-hook 'my-c-mode-hook)

;; rtags / cmake-ide
(require 'rtags)
(cmake-ide-setup)

;; irony
(add-hook 'c++-mode-hook 'irony-mode)
(add-hook 'c-mode-hook 'irony-mode)
(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)
