;;; Org mode defaults
(require 'org)
(require 'org-agenda)
(setq org-agenda-include-diary t)
(setq calendar-week-start-day 0
      calendar-day-name-array ["Domingo" "Segunda" "Terça" "Quarta"
			       "Quinta" "Sexta" "Sábado"]
      calendar-month-name-array ["Janeiro" "Fevereiro" "Março" "Abril"
				 "Maio" "Junho" "Julho" "Agosto"
				 "Setembro" "Outubro" "Novembro" "Dezembro"])

(add-to-list 'org-agenda-custom-commands
	     '("Y" "Agenda anual de aniversários e feriados" agenda "Visão Anual"
	       ((org-agenda-span 365)
		(org-agenda-filter-by-category 'Aniversário)
		(org-agenda-time-grid nil))))
(add-to-list 'org-agenda-custom-commands
	     '("1" "Agenda mensal" agenda "Visão Mensal"
	       ((org-agenda-span 31)
		(org-agenda-time-grid nil))))
(add-to-list 'org-agenda-custom-commands
	     '("7" "Agenda dos próximos sete dias" agenda "Visão de Sete Dias"
	       ((org-agenda-span 7)
		(org-agenda-time-grid nil))))

;; Text wrapping
(add-hook 'org-mode-hook #'toggle-word-wrap)
(add-hook 'org-mode-hook #'org-indent-mode)

;;; EXTERNAL: brazil-holidays.el
(load (expand-file-name "~/.emacs.d/elisp/brazil-holidays.el"))
(setq calendar-holidays holiday-brazil-all)


;;; org-alert
(require 'org-alert)
(setq alert-default-style 'libnotify)
(setq org-alert-notification-title "*org-mode*")
(setq org-alert-interval 21600)
(org-alert-enable)


;;; calfw / calfw-org
(require 'calfw)
(require 'calfw-org)


;;; org-journal
;;; org-journal-dir needs to be defined in custom.el
(setq org-journal-loaded nil)
(defun org-journal-load-files ()
  (interactive)
  (when (not org-journal-loaded)
    (require 'org-journal)
    (setq org-agenda-file-regexp "\\`[^.].*\\.org'\\|[0-9]$")
    (add-to-list 'org-agenda-files org-journal-dir)
    (setq org-journal-loaded t)))


;;; Export to PDF/LaTeX: Setup
(require 'ox-latex)
(unless (boundp 'org-latex-classes)
  (setq org-latex-classes nil))
(add-to-list 'org-latex-classes
	     '("abntex2"
	       "\\documentclass{abntex2}
                [NO-DEFAULT-PACKAGES]
                [EXTRA]"
	       ("\\section{%s}" . "\\section*{%s}")
	       ("\\subsection{%s}" . "\\subsection*{%s}")
	       ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
	       ("\\paragraph{%s}" . "\\paragraph*{%s}")
	       ("\\subparagraph{%s}" . "\\subparagraph*{%s}")
	       ("\\maketitle" . "\\imprimircapa")))
(setq org-export-allow-bind-keywords t)

;; Prevent htmlize usage of current theme
(setq org-html-htmlize-output-type 'css)

;; Preserve indentation on export
(setq org-src-preserve-indentation t)

