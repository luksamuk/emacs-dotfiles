;; indent-guide
(indent-guide-global-mode)

;; highlight-numbers
(add-hook 'prog-mode-hook 'highlight-numbers-mode)

;; pretty-mode
;;(require 'pretty-mode)
;;(global-pretty-mode t)
