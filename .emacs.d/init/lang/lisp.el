;; Using quicklisp-slime-helper
;;(load (expand-file-name "~/quicklisp/slime-helper.el"))

;; Manual SLIME installation
(add-to-list 'load-path (expand-file-name "~/git/3rdparty/slime"))
(require 'slime-autoloads)
(setq slime-contribs '(slime-fancy))

;; SLY installation
;; (add-to-list 'load-path "~/git/3rdparty/sly")
;; (require 'sly-autoloads)

;; CL implementation
(setq inferior-lisp-program "sbcl --noinform --no-linedit")

;; Prettify symbols.
;; Also do the same for elisp and scheme.
(add-hook 'lisp-mode-hook #'prettify-symbols-mode)
(add-hook 'emacs-lisp-mode-hook #'prettify-symbols-mode)
(add-hook 'scheme-mode-hook #'prettify-symbols-mode)

;; Parinfer
(add-hook 'lisp-mode-hook #'parinfer-mode)
(add-hook 'emacs-lisp-mode-hook #'parinfer-mode)
(add-hook 'scheme-mode-hook #'parinfer-mode)

;; Scheme config
(setq geiser-default-implementation 'chez)

