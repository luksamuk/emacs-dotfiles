;;; Font
(defvar my-default-font "-b&h-lucidatypewriter-medium-r-normal-sans-12-*-*-*-*-*-iso8859-1")

;; Daemon
(defvar my-frame-alist
  '((font       . "-b&h-lucidatypewriter-medium-r-normal-sans-12-*-*-*-*-*-iso8859-1")
    (scroll-bar . -1)
    (height     . 50)
    (width      . 95)
    (vertical-scroll-bars)))
(when (daemonp)
  (setq default-frame-alist my-frame-alist))


;;; General
(setq inhibit-startup-screen t)
(setq inhibit-splash-screen t)
(show-paren-mode 1)
(setq show-paren-delay 0)
(menu-bar-mode -99)
(global-linum-mode 1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
;;(setq browser-url-browser-function 'browse-url-firefox)
(setq browser-url-browser-function 'browse-url-chromium)
(setq linum-format "%5d")



(set-default-font my-default-font)



;;; Mouse
(setq transient-mark-mode t)
(xterm-mouse-mode)
(setq mouse-wheel-follow-mouse t)

;;; Scrolling
(setq mouse-wheel-follow-mouse 't)
(setq scroll-step 1)
(global-set-key (kbd "<mouse-4>") 'scroll-down-line)
(global-set-key (kbd "<mouse-5>") 'scroll-up-line)
(global-set-key (kbd "<C-mouse-4>") 'scroll-down-command)
(global-set-key (kbd "<C-mouse-5>") 'scroll-up-command)

;;; Eshell
(add-hook 'eshell-preoutput-filter-functions 'ansi-color-apply)

;;; Autocompletion
(setq tab-always-indent 'complete)
(add-to-list 'completion-styles 'initials t)


;;; Company
(require 'company)
(add-hook 'after-init-hook 'global-company-mode)


;; Initialization string on scratch buffer
;;(setq initial-scratch-message ";; Scratch buffer for Elisp eval.\n\n")
(setq initial-scratch-message
      (concat
       ";;\n"
       ";; \"We toast the Lisp programmer who pens his thoughts\n"
       ";;  within nests of parentheses.\" -- Alan J. Perlis\n"
       ";;\n"
       ";;		   .......\n"
       ";;		 ...	 ...\n"
       ";;			   ...\n"
       ";;			    ..\n"
       ";;			    ..\n"
       ";;			  ....\n"
       ";;		       .... ...\n"
       ";;		      ..    ..\n"
       ";;		    ...	    ...\n"
       ";;		 ....	    ....     ..\n"
       ";;		...	       .......\n"
       ";;\n;;\n"
       ";;            HACKS AND GLORIES AWAIT!\n"
       ";;\n\n"))
;;(setq initial-major-mode 'lisp-mode) ;; Start with Common Lisp

;; Spaceline (Spacemacs standalone modeline)
;;(require 'spaceline-config)
;;(spaceline-emacs-theme)
