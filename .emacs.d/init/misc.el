;; Reload .dir-locals.el for current buffer
(defun reload-dir-locals-for-current-buffer ()
  "reload dir locals for the current buffer"
  (interactive)
  (let ((enable-local-variables :all))
    (hack-dir-local-variables-non-file-buffer)))

;; mpdel configuration
;;(add-to-list 'load-path "~/.emacs.d/lib/mpdel")
;;(require 'mpdel)
;;(mpdel-mode)

;; Set generic browser (in this case, qutebrowser)
(defun browser-set-qute ()
  (interactive)
  (setq browse-url-browser-function 'browse-url-generic)
  (setq browse-url-generic-program "qutebrowser")
  (setq browse-url-generic-args nil))

(defun browser-set-firefox ()
  (interactive)
  (setq browse-url-browser-function 'browse-url-firefox))


;; Quickview.
;; Link: https://emacs.stackexchange.com/questions/275/dired-quick-view-key-to-preview-the-file-at-point


(defun dired-find-file-until-key ()
  (interactive)
  (let ((filename (dired-file-name-at-point))
    (buffer-count (length (buffer-list))))
    (dired-find-file)
    (message "Showing %s temporarily..." filename)
    ;;(isearch-unread-key-sequence (list (read-event)))
    (isearch-unread (read-event))
    (if (= (length (buffer-list)) buffer-count)
    (bury-buffer)
      (kill-buffer))))

(eval-after-load "dired"
  '(define-key dired-mode-map (kbd "C-M-v") 'dired-find-file-until-key))


;; better-shell
(global-set-key (kbd "C-\"") 'better-shell-shell)
(global-set-key (kbd "C-:")  'better-shell-remote-open)

;; Treemacs stuff
(require 'treemacs)
(setq treemacs-directory-face my-default-font
      treemacs-directory-collapsed-face my-default-font
      treemacs-root-face my-default-font
      treemacs-tags-face my-default-font)

;; Smooth scrolling stuff
(setq scroll-conservatively 101) ;; move minimum when cursor exits view, instead of recentering
(setq mouse-wheel-scroll-amount '(1)) ;; mouse scroll moves 1 line at a time, instead of 5 lines
(setq mouse-wheel-progressive-speed nil) ;; on a long mouse scroll keep scrolling by 1 line


;; Autocomplete
(ac-config-default)
(global-auto-complete-mode t)

