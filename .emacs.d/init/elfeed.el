;;; elfeed / elfeed-org
(global-set-key (kbd "C-x w") 'elfeed)
(elfeed-org)

(setq-default elfeed-search-filter "@3-days-ago +unread ")

;; Move forward and backward
(define-key elfeed-show-mode-map (kbd "C-<right>") 'elfeed-show-next)
(define-key elfeed-show-mode-map (kbd "C-<left>") 'elfeed-show-prev)
(define-key elfeed-show-mode-map (kbd "k") 'elfeed-show-next)
(define-key elfeed-show-mode-map (kbd "j") 'elfeed-show-prev)
