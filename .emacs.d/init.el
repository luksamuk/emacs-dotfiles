(load (expand-file-name "~/.emacs.d/init/packaging.el"))
(package-initialize)


(setq custom-file "~/.emacs.d/init/custom.el")
(load custom-file)

(load (expand-file-name "~/.emacs.d/init/appearance.el"))
(load (expand-file-name "~/.emacs.d/init/misc.el"))

;;; Languages
(load (expand-file-name "~/.emacs.d/init/lang/global.el"))
(load (expand-file-name "~/.emacs.d/init/lang/lisp.el"))
(load (expand-file-name "~/.emacs.d/init/lang/rust.el"))
(load (expand-file-name "~/.emacs.d/init/lang/golang.el"))
(load (expand-file-name "~/.emacs.d/init/lang/org.el"))
(load (expand-file-name "~/.emacs.d/init/lang/nasm.el"))
(load (expand-file-name "~/.emacs.d/init/lang/c-cpp.el"))
(load (expand-file-name "~/.emacs.d/init/lang/latex.el"))
(load (expand-file-name "~/.emacs.d/init/lang/web-related.el"))

;;; Other features
(load (expand-file-name "~/.emacs.d/init/elfeed.el"))
(setq debug-on-error t)
